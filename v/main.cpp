#include "main.hpp"

void VulkanTriangle::Run() 
{
	InitWindow();
	InitVulkan();
	MainLoop();
	Cleanup();
}

void VulkanTriangle::InitWindow() 
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = glfwCreateWindow(800, 600, "VK", nullptr, nullptr);
}

void VulkanTriangle::InitVulkan()
{
	if (enableValidationLayers && !CheckValidationLayersSupport())
	{
		throw std::runtime_error("Couldn't enable debug layers");
	}
	else
	{
		std::cout << "Successfully enabled debug validation layers\n";
	}

	uint32_t extensionsCount = 0;
	const char** extensionNames;

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "VK Triangle";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "No engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_1;

	extensionNames = glfwGetRequiredInstanceExtensions(&extensionsCount);

	std::cout << "There are " << extensionsCount << " extensions" << std::endl;

	std::vector<VkExtensionProperties> extensions(extensionsCount);

	vkEnumerateInstanceExtensionProperties(nullptr, &extensionsCount, extensions.data());

	std::cout << "Available extensions: " << std::endl;
	for (const auto& e : extensions)
	{
		std::cout << "\t" << e.extensionName << std::endl;
	}

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = extensionsCount;
	createInfo.ppEnabledExtensionNames = extensionNames;

	if (enableValidationLayers)
	{
		createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		createInfo.ppEnabledLayerNames = validationLayers.data();
	}
	else
	{
		createInfo.enabledLayerCount = 0;
	}

	if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to initialise vk");
	}
}

bool VulkanTriangle::CheckValidationLayersSupport()
{
	uint32_t layerCount = 0;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availableLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	for (const char* layerName : validationLayers)
	{
		bool layerFound = false;

		for (const auto& layerProperties : availableLayers)
		{
			if (!!strcmp(layerName, layerProperties.layerName))
			{
				layerFound = true;
				break;
			}
		}

		if (!layerFound) return false;
	}

	return true;
}

void VulkanTriangle::MainLoop() 
{
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
	}
}

void VulkanTriangle::Cleanup() 
{
	vkDestroyInstance(instance, nullptr);
	glfwDestroyWindow(window);
	glfwTerminate();
}

int main()
{
	VulkanTriangle t = VulkanTriangle();
	t.Run();
	return VK_SUCCESS;
}